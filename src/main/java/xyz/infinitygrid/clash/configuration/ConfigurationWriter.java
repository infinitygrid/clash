package xyz.infinitygrid.clash.configuration;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import static xyz.infinitygrid.clash.configuration.ConfigurationField.*;

/**
 * Writes values into a configuration file
 */
public class ConfigurationWriter {

    private final FileConfiguration config;

    ConfigurationWriter(FileConfiguration config) {
        this.config = config;
    }

    /**
     * Sets all location values under a specified path
     * @param path Path to set the location to
     * @param loc Location to set
     */
    public void setLocation(String path, Location loc) {
        config.set(path + "." + WORLD_UID, loc.getWorld().getUID().toString());
        config.set(path + "." + LOC_X, loc.getX());
        config.set(path + "." + LOC_Y, loc.getY());
        config.set(path + "." + LOC_Z, loc.getZ());
        config.set(path + "." + LOC_YAW, loc.getYaw());
        config.set(path + "." + LOC_PITCH, loc.getPitch());
    }

}
