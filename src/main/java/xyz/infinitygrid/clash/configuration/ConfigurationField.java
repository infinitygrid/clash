package xyz.infinitygrid.clash.configuration;

/**
 * Represents single, default fields for the use inside configuration files
 */
public enum ConfigurationField {

    WORLD_UID("World UID"),
    LOC_X("X"),
    LOC_Y("Y"),
    LOC_Z("Z"),
    LOC_YAW("Yaw"),
    LOC_PITCH("Pitch");

    private String path;

    ConfigurationField(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return path;
    }

}
