package xyz.infinitygrid.clash.configuration;

import java.io.File;

/**
 * Represents the configuration file for match signs
 */
public class CfgSigns extends Configuration {

    @Override
    File getSavePath() {
        return new File(dataFolder + "/signs.yml");
    }



}
