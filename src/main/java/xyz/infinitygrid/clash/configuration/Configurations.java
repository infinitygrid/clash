package xyz.infinitygrid.clash.configuration;

import java.util.HashMap;

public class Configurations {

    private static HashMap<Class, Configuration> classConfigurationHashMap = new HashMap<>();

    /**
     * Registers configurations
     * @param configurations Configurations to register
     */
    public static void register(Configuration... configurations) {
        for(Configuration configuration : configurations) {
            if(configuration.getSavePath() != null) {
                classConfigurationHashMap.put(configuration.getClass(), configuration);
            } else {
                throw new NullPointerException("Save path cannot be null");
            }
        }
    }

    /**
     * Get configuration by class
     * @param c Configuration class
     * @return Configuration class; if not existing: null
     */
    public static Configuration getConfig(Class c) {
        return classConfigurationHashMap.get(c);
    }

}
