package xyz.infinitygrid.clash.configuration;

/**
 * Creates a path for configuration i.e. 'CLASH.Locations.Lobby'
 */
public class ConfigurationPath {

    private final ConfigurationField[] configurationFields;
    private final Character defaultDividerCharacter = '.';

    ConfigurationPath(ConfigurationField... configurationFields) {
        this.configurationFields = configurationFields;
    }

    /**
     * @return configuration path as string
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < configurationFields.length; i++) {
            stringBuilder.append(configurationFields[i].toString());
            if(i != configurationFields.length - 1) stringBuilder.append(defaultDividerCharacter);
        }
        return stringBuilder.toString();
    }
}
