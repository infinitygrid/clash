package xyz.infinitygrid.clash.configuration;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import xyz.infinitygrid.clash.CLASH;

import java.io.File;
import java.io.IOException;

/**
 * Represents a configuration file
 */
public abstract class Configuration {

    private FileConfiguration config;
    public String dataFolder = CLASH.instance.getDataFolder().toString();

    /**
     * The constructor will automatically create a new configuration file, if it doesn't exist under specified path
     */
    Configuration() {
        if(!getSavePath().exists()) {
            CLASH.sendConsoleMessage("Couldn't find configuration file '" + getSavePath().getName() + "'; creating new one...");
            config = getNewConfigFile();
            afterCreation();
        } else {
            config = YamlConfiguration.loadConfiguration(getSavePath());
        }
    }

    /**
     * @return save path of configuration file
     */
    abstract File getSavePath();

    /**
     * Invoked after the creation of a config file. Useful for modifying the configuration file after its creation: i.e.
     * add fields and values
     */
    void afterCreation() {

    }

    /**
     * @return new configuration file (FileConfiguration) under specified save path
     */
    private FileConfiguration getNewConfigFile() {
        FileConfiguration newConfig = new YamlConfiguration();
        try {
            newConfig.save(getSavePath());
            return newConfig;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Saves the configuration
     */
    public void saveConfig() {
        try {
            config.save(getSavePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return ConfigurationWriter
     */
    public ConfigurationWriter writer() {
        return new ConfigurationWriter(config);
    }

    /**
     * @return ConfigurationReader
     */
    public ConfigurationReader reader() {
        return new ConfigurationReader(config);
    }



}
