package xyz.infinitygrid.clash.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;

/**
 * Represents the configuration file for locations
 */
public class CfgLocations extends Configuration {

    @Override
    File getSavePath() {
        return new File(dataFolder + "/locations.yml");
    }

    @Override
    void afterCreation() {
        writer().setLocation("Spawn", new Location(Bukkit.getWorlds().get(0), 0, 170, 0, 0, 0));
        saveConfig();
    }

}
