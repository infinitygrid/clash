package xyz.infinitygrid.clash.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.UUID;

/**
 * Reads values from configuration file
 */
public class ConfigurationReader {

    private final FileConfiguration config;

    ConfigurationReader(FileConfiguration config) {
        this.config = config;
    }

    /**
     * Gets a {@code Location} that was set by {@link ConfigurationWriter#setLocation(String, Location)} from specified {@code path}
     * @param path Path to get location from
     */
    public Location getLocation(String path) {
        return new Location(
                Bukkit.getWorld(UUID.fromString(config.getString(path + "." + ConfigurationField.WORLD_UID))),
                config.getDouble(path + "." + ConfigurationField.LOC_X),
                config.getDouble(path + "." + ConfigurationField.LOC_Y),
                config.getDouble(path + "." + ConfigurationField.LOC_Z),
                (float) config.getDouble(path + "." + ConfigurationField.LOC_YAW),
                (float) config.getDouble(path + "." + ConfigurationField.LOC_PITCH)
        );
    }



}
