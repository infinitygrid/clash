package xyz.infinitygrid.clash.match;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.item.ClashItem;
import xyz.infinitygrid.clash.item.FoodItem;
import xyz.infinitygrid.clash.item.Grenade;
import xyz.infinitygrid.clash.item.Weapon;
import xyz.infinitygrid.clash.player.CLPlayer;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.bukkit.Material.*;

/**
 * All behaviour and data independent of match state.
 *
 * This includes associated signs, world, loaded map, players, ...
 */
public class MatchContext {
    private Match match;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public MatchContext() {
        // TODO: more match info (see above)
        match = new Match.Waiting(this);
    }

    private BukkitTask tickTask = null;

    private void startTheClock() {
        tickTask = new BukkitRunnable() {
            @Override
            public void run() {
                match.tick();
            }
        }.runTaskTimerAsynchronously(CLASH.instance, 0, 1);
    }

    private void stopTheClock() {
        if (tickTask != null && !tickTask.isCancelled()) {
            tickTask.cancel();
            tickTask = null;
        }
    }

    // Putting this here because putting it inside ClashItem could result in
    // JRE deadlock while loading classes (according to IDEA).
    // Also, the specification of items might be better off around here because
    // different match types possibly having different item sets in the future
    public static final Set<ClashItem> ITEMS = Stream.of(
            new Weapon("Wooden Sword", WOOD_SWORD, (byte) 10, (short) 10),
            new Weapon("Stone Sword", STONE_SWORD, (byte) 7, (short) 12),
            new Weapon("Golden Sword", GOLD_SWORD, (byte) 5, (short) 14),
            new Weapon("Iron Sword", IRON_SWORD, (byte) 3, (short) 16),
            new Weapon("Diamond Sword", DIAMOND_SWORD, (byte) 1, (short) 18),

            new FoodItem("Bread", BREAD, (byte) 10, (short) 10),
            new FoodItem("Delicious Steak", COOKED_BEEF, (byte) 10, (short) 10),
            new FoodItem("Chicken", COOKED_CHICKEN, (byte) 10, (short) 10),
            new FoodItem("Apple", APPLE, (byte) 10, (short) 10),

            new Grenade("Frag Grenade", FIREBALL, (byte) 6, 40, 6)

    ).collect(Collectors.toSet());

    private HashSet<CLPlayer> players = new HashSet<>();

    /**
     * @return the number of players in this match, participating or not
     */
    public int getPlayerCount() {
        assert players != null;
        return players.size();
    }

    /**
     * @return an iterable whose iterator will return all players,
     * participating or not, in this match
     */
    public Iterable<CLPlayer> getPlayers() {
        assert players != null;
        return players;
    }
}
