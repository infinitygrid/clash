package xyz.infinitygrid.clash.match;

import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.player.CLPlayer;

import java.util.HashSet;
import java.util.Set;

/**
 * State/status-dependent behaviour of matches.
 */
abstract class Match {

    protected final MatchContext context;

    Match(MatchContext context) {
        assert context != null;
        this.context = context;
    }

    /**
     * This function is called asynchronously on every tick, in every match
     * context.
     */
    abstract void tick();

    /**
     * Handles player death.
     * @param clp the player who died
     */
    abstract void death(CLPlayer clp);

    // ====================================
    // ==  Match states/implementations  ==
    // ====================================

    static class Waiting extends Match {

        private static final int TOTAL_COUNTDOWN_TIME = 30 * 20;
        private int timeToStart = TOTAL_COUNTDOWN_TIME;

        Waiting(MatchContext context) {
            super(context);
            for (CLPlayer clp : context.getPlayers()) {
                final Player p = clp.getBukkitPlayer();
                p.getInventory().clear();
                /* TODO:
                - respawn player in lobby
                 */
            }
        }

        @Override
        void tick() {
            if (context.getPlayerCount() <= 1) {
                // don’t count down unless there’s at least two players
                timeToStart = TOTAL_COUNTDOWN_TIME;
            } else {
                if (timeToStart > 0) {
                    // wait a bit more
                    --timeToStart;
                } else {
                    // let’s go!
                    context.setMatch(new Running(context));
                }
            }
        }

        @Override
        void death(CLPlayer clp) {
            // TODO: respawn in lobby
        }
    }

    static class Running extends Match {

        private static final int TOTAL_MATCH_TIME = 10 * 60 * 20;
        private int timeRemaining = TOTAL_MATCH_TIME;

        /**
         * All players in this match, dead or alive, present or not.
         */
        private Set<CLPlayer> participants = new HashSet<>();

        /**
         * All players in this match who are present and alive.
         */
        private Set<CLPlayer> playing = new HashSet<>();

        Running(MatchContext context) {
            super(context);
            for (CLPlayer clp : context.getPlayers()) {
                participants.add(clp);
                playing.add(clp);
                clp.setDamage(0);
                final Player p = clp.getBukkitPlayer();
                /* TODO:
                - spawn player at an appropriate spawn point
                - initialise health
                */
            }
        }

        @Override
        void tick() {
            if (playing.size() <= 1 || timeRemaining <= 0 || context.getPlayerCount() <= 1) {
                context.setMatch(new Ending(context));
            } else {
                --timeRemaining;
                maybeSpawnItem();
            }
        }

        /**
         * Possibly spawns a random item at a random location.
         */
        private void maybeSpawnItem() {
            // TODO: NYI
        }

        @Override
        void death(CLPlayer clp) {
            /* TODO:
            - announce death
            - update health
                - if 0, remove player from `playing`
            - respawn the player
            */
        }

    }

    static class Ending extends Match {

        private static final int TOTAL_END_TIME = 15 * 20;
        private int timeToActualEnd = TOTAL_END_TIME;

        Ending(MatchContext context) {
            super(context);
        }

        @Override
        void tick() {
            if (timeToActualEnd <= 0) {
                context.setMatch(new Waiting(context));
            } else {
                --timeToActualEnd;
            }
        }

        @Override
        void death(CLPlayer clp) {
            // what to do here?
            // Respawn inside map or in map lobby?
            // Hmm...
        }
    }

}
