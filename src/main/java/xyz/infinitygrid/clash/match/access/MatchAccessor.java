package xyz.infinitygrid.clash.match.access;

import org.bukkit.World;
import org.bukkit.entity.Player;

abstract class MatchAccessor {

    private final World matchWorld;

    MatchAccessor(World world) {
        this.matchWorld = world;
    }

    public void teleport(Player p) {
        // TODO: Teleport player to the match world's lobby
    }

}
