package xyz.infinitygrid.clash.player;

import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Responsible for registering and unregistering CLASH players
 */
public class PlayerRegistry {

    /**
     * List of registered CLASH players
     */
    private static final HashMap<Player, CLPlayer> clPlayers = new HashMap<>();

    /**
     * Registers a CLASH player
     * @param bukkitPlayer Player
     */
    public static void registerCLPlayer(Player bukkitPlayer) {
        clPlayers.put(bukkitPlayer, new CLPlayer(bukkitPlayer));
    }

    /**
     * Unregisters a CLASH player
     * @param clashPlayer CLASH player
     */
    public static void unregisterCLPlayer(CLPlayer clashPlayer) {
        clPlayers.remove(clashPlayer.getBukkitPlayer());
    }

    /**
     * Unregisters a CLASH player
     * @param bukkitPlayer Player
     */
    public static void unregisterCLPlayer(Player bukkitPlayer) {
        clPlayers.remove(bukkitPlayer);
    }

    /**
     * @param bukkitPlayer Bukkit Player
     * @return CLASH Player
     */
    public static CLPlayer getCLPlayer(Player bukkitPlayer) {
        final CLPlayer clp = clPlayers.get(bukkitPlayer);
        if (clp == null)
            throw new IllegalArgumentException(
                    "Player" + bukkitPlayer.getDisplayName() +
                            "was not registered with Clash."
            );
        return clp;
    }

}
