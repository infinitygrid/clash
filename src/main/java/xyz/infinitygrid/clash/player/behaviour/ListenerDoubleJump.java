package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import xyz.infinitygrid.clash.effects.sound.SoundData;
import xyz.infinitygrid.clash.effects.sound.SoundFX;
import xyz.infinitygrid.clash.player.CLPlayer;
import xyz.infinitygrid.clash.player.PlayerRegistry;

import static org.bukkit.ChatColor.RED;

/**
 * Responsible for giving the player the mechanics to access their DoubleJump
 */
public class ListenerDoubleJump implements Listener {

    private static SoundData jumpSound = new SoundData(Sound.ENTITY_ZOMBIE_INFECT, 1f, 2f);
    private static SoundData errorSound = new SoundData(Sound.BLOCK_NOTE_BASS, 2f, .8f);

    @EventHandler
    private void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        CLPlayer clp = PlayerRegistry.getCLPlayer(p);
        DoubleJump dj = clp.getDoubleJump();
        Energy en = clp.getEnergy();

        if (clp.hasGamemodeWithFlyingAbility() || !dj.isEnabled()) return;

        if (p.isFlying()) {
            if (dj.isJumpEnabled()) {
                dj.disableJump();
                if (en.isFull()) {
                    dj.jump();
                    // shouldn’t the jumping sound be played to everyone?
                    jumpSound.play(p);
                } else {
                    errorSound.play(p);
                    p.sendMessage(RED + "Your double jump is charging!");
                }
            } else {
                errorSound.play(p);
                p.setFlying(false);
            }
        } else if (p.isOnGround()) {
            dj.enableJump();
        } else if (clp.isSneaking()) {
            if (!dj.isBoostedDownwards() && en.isFull()) {
                dj.dash();
                new SoundFX(p).play(
                        new SoundData(Sound.ENTITY_ENDERDRAGON_FLAP, 1f, 1f),
                        new SoundData(Sound.ENTITY_GENERIC_EXPLODE, .05f, 1f)
                );
            }
        }

    }

}
