package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.util.Vector;

public class ListenerDeath implements Listener {

    @EventHandler private void onDeath(PlayerDeathEvent e) {
        e.setKeepInventory(true);
        e.setKeepLevel(true);
        e.setDroppedExp(0);
        e.setDeathMessage(null);
        Player p = e.getEntity();
        Vector vel = p.getVelocity();
        Location deathLoc = p.getLocation();
        p.spigot().respawn();
        p.teleport(deathLoc);
        p.setVelocity(vel);
    }

}
