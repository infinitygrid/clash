package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import xyz.infinitygrid.clash.player.CLPlayer;
import xyz.infinitygrid.clash.player.PlayerRegistry;

public class ListenerToggleSneak implements Listener {

    @EventHandler
    private void sneaking(PlayerToggleSneakEvent e) {
        final CLPlayer clp = PlayerRegistry.getCLPlayer(e.getPlayer());
        clp.setSneaking(e.isSneaking());
        e.setCancelled(true);
    }

}
