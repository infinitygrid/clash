package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.player.CLPlayer;
import xyz.infinitygrid.clash.player.PlayerRegistry;

/**
 * Listens for gamemode changes and toggles double jump accordingly to new gamemode
 */
public class ListenerGamemode implements Listener {

    @EventHandler
    private void onGamemodeChange(PlayerGameModeChangeEvent e) {
        CLPlayer clPlayer = PlayerRegistry.getCLPlayer(e.getPlayer());
        DoubleJump doubleJump = clPlayer.getDoubleJump();
        Bukkit.getScheduler().runTaskLater(CLASH.instance, () -> {
            if (clPlayer.hasGamemodeWithFlyingAbility())
                doubleJump.disable();
            else
                doubleJump.enable();
        }, 1);
    }

}
