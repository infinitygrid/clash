package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import static org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason.CUSTOM;

public class ListenerRegainHealth implements Listener {

    @EventHandler private void onPlayerRegen(EntityRegainHealthEvent e) {
        if(e.getRegainReason() != CUSTOM) e.setCancelled(true);
    }

}
