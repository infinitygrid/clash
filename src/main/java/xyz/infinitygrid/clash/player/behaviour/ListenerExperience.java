package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;

public class ListenerExperience implements Listener {

    @EventHandler private void onExp(PlayerExpChangeEvent e) {
        e.setAmount(0);
    }

    @EventHandler private void onLevel(PlayerLevelChangeEvent e) {
        Player p = e.getPlayer();
        p.setLevel(e.getOldLevel());
        p.setExp(1F);
    }

}
