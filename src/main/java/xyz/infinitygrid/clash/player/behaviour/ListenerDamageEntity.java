package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ListenerDamageEntity implements Listener {

    @EventHandler private void onDamage(EntityDamageByEntityEvent e) {
        LivingEntity enDamaged = (LivingEntity) e.getEntity();
        LivingEntity enDamager = (LivingEntity) e.getDamager();
        if(enDamager instanceof Player) {
            Player pDamager = (Player) enDamager;
            if(enDamaged instanceof Player) {
                Player pDamaged = (Player) enDamaged;
                e.setDamage(0D);
            }
        }
    }

}
