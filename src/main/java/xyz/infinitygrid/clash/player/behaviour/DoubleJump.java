package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.effects.WorldFX;
import xyz.infinitygrid.clash.effects.sound.SoundData;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * Player double jump
 */
public class DoubleJump {

    private final Player p;
    private final CLPlayer clPlayer;
    private boolean enabled = false;
    private final WorldFX.MovementTrail movementTrail;
    private final WorldFX.Shockwave shockwave;
    private boolean boostedDownwards;
    private static final double smashingRadius = 4;
    boolean jump;

    private static final SoundData smashSound = new SoundData(Sound.ENTITY_GENERIC_EXPLODE, 1f, 1f);

    /**
     * Makes a CLASH-player able to double jump
     * @param clPlayer CLASH player
     */
    public DoubleJump(CLPlayer clPlayer) {
        p = clPlayer.getBukkitPlayer();
        this.clPlayer = clPlayer;
        movementTrail = new WorldFX.MovementTrail(p);
        shockwave = new WorldFX.Shockwave(p);
        jump = true;
    }

    /**
     * @return CLASH Player
     */
    public CLPlayer getCLPlayer() {
        return clPlayer;
    }

    /**
     * Disables double jump.
     */
    public void disable() {
        enabled = false;
        p.setFlySpeed(0.2F);
    }

    /**
     * Enables double jump.
     */
    public void enable() {
        enabled = true;
        p.setFlySpeed(0F);
        p.setAllowFlight(true);
    }

    /**
     * @return whether double jump is enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Makes the player jump
     */
    public void jump() {
        p.setVelocity(p.getEyeLocation().getDirection().setY(1));
        movementTrail.onIncreasingY(Particle.CLOUD, 1, .1, 2);
    }

    public boolean isJumpEnabled() {
        return jump;
    }

    public void enableJump() {
        jump = true;
    }

    public void disableJump() {
        jump = false;
        p.setFlying(false);
    }

    public boolean isBoostedDownwards() {
        return boostedDownwards;
    }

    /**
     * Execute the stomp attack at current position
     */
    private void smash() {
        shockwave.make(p.getLocation(), Particle.CRIT_MAGIC,
                0, 3, 10, 1, 0, 0.3F, 0.2F, false);
        p.spawnParticle(Particle.EXPLOSION_LARGE, p.getLocation().add(0, 2, 0), 5, 1, 2, 1);
        Location pLoc = p.getLocation();
        for(Entity e : p.getNearbyEntities(smashingRadius, smashingRadius * 2, smashingRadius)) {
            Location enLoc = e.getLocation();
            if(pLoc.getY() - enLoc.getY() > -smashingRadius) {
                double dist = pLoc.distance(enLoc);
                if(dist <= smashingRadius) {
                    Vector kb = enLoc.toVector().subtract(pLoc.toVector());
                    if (e instanceof Player) {
                        // TODO: Include logic for players in matches
                    }
                    e.setVelocity(kb);
                }
            }
        }
        smashSound.play(p);
    }

    /**
     * Ensure that the player is dashing.
     *
     * If the player is already dashing, nothing happens. If not, the player
     * starts dashing and will, if they are sneaking when hitting the
     * ground, stomp.
     */
    public void dash() {
        if (boostedDownwards)
            return;
        boostedDownwards = true;
        p.setVelocity(p.getEyeLocation().getDirection().multiply(0.8).setY(-.8));
        p.spawnParticle(Particle.EXPLOSION_LARGE, p.getLocation(), 1);
        new BukkitRunnable() {
            @Override
            public void run() {
                p.spawnParticle(Particle.CRIT_MAGIC, p.getLocation(), 10, 0, 0, 0, 0.3D);
                if (p.isOnGround()) {
                    cancel();
                    boostedDownwards = false;
                    if (clPlayer.isSneaking() &&
                            clPlayer.getEnergy().takeEnergy(1000)) {
                        smash();
                    }
                }
            }
        }.runTaskTimer(CLASH.instance, 0, 1);
    }

}
