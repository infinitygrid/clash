package xyz.infinitygrid.clash.player.behaviour;

import org.bukkit.scheduler.BukkitRunnable;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.effects.GUIFX;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * A class responsible for a players energy displayed in their experience bar
 */
public class Energy {

    private int energy = 1000;
    private final CLPlayer clPlayer;
    private boolean energyRecharge;
    private final GUIFX.ExperienceBarTransition xpfx;

    /**
     * Make a CLASH player able to have energy
     * @param clPlayer CLASH Player
     */
    public Energy(CLPlayer clPlayer) {
        this.clPlayer = clPlayer;
        xpfx = new GUIFX.ExperienceBarTransition(clPlayer.getBukkitPlayer());
        energyRecharge = false;
        displayEnergy();
    }

    /**
     * Displays the current energy in their experience bar
     */
    private void displayEnergy() {
        if (!xpfx.inProgress())
            clPlayer.getBukkitPlayer().setExp(.001F * energy);
    }

    private boolean hasEnergy(int en) {
        return energy >= en;
    }

    boolean takeEnergy(int toTake) {
        if (hasEnergy(toTake)) {
            xpfx.make(.001F * energy, 0.256F, 1, 1);
            energy -= toTake;
            rechargeEnergy();
            displayEnergy();
            return true;
        }
        return false;
    }

    /**
     * Start the energy recharging logic and FX, regardless of whether it is
     * already running.
     */
    private void rechargeEnergy() {
        if (!energyRecharge) {
            energyRecharge = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (energy < 1000) {
                        energy += 5;
                    } else {
                        energy = 1000;
                        cancel();
                        energyRecharge = false;
                    }
                    displayEnergy();
                }
            }.runTaskTimer(CLASH.instance, 0, 1);
        }
    }

    /**
     * @return whether energy is full
     */
    boolean isFull() {
        return energy >= 1000;
    }
}
