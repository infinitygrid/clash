package xyz.infinitygrid.clash.player;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Listens for a join or quit and accordingly registers or unregisters a CLASH player
 */
public class ListenerConnection implements Listener {

    @EventHandler
    private void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        PlayerRegistry.registerCLPlayer(p);
        p.setFoodLevel(20);
        p.setMaximumNoDamageTicks(20);
        p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);
    }

    @EventHandler
    private void onQuit(PlayerQuitEvent e) {
        PlayerRegistry.unregisterCLPlayer(e.getPlayer());
    }

}
