package xyz.infinitygrid.clash.player;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xyz.infinitygrid.clash.configuration.CfgLocations;
import xyz.infinitygrid.clash.configuration.Configurations;
import xyz.infinitygrid.clash.player.behaviour.DoubleJump;
import xyz.infinitygrid.clash.player.behaviour.Energy;

/**
 * Represents a player currently on the Clash server.
 */
public class CLPlayer {

    private final Player p;
    private int damage;
    private final DoubleJump doubleJump;
    private final Energy energy;
    private int lives = 3;
    private int maxLives = 3;

    private boolean isSneaking = false;

    public CLPlayer(Player p) {
        assert p != null;
        this.p = p;
        doubleJump = new DoubleJump(this);
        energy = new Energy(this);
        if(!hasGamemodeWithFlyingAbility()) {
            doubleJump.enable();
        } else {
            p.setFlySpeed(.2F);
        }
    }

    /**
     * @return the Player object for this player
     */
    public Player getBukkitPlayer() {
        return p;
    }

    /**
     * Teleports player to the spawn defined in locations.yml
     */
    public void teleportToSpawn() {
        p.teleport(Configurations.getConfig(CfgLocations.class).reader().getLocation("Spawn"));
    }

    /**
     * @return Returns true when player has creative or spectator mode
     */
    public boolean hasGamemodeWithFlyingAbility() {
        return p.getGameMode() == GameMode.CREATIVE || p.getGameMode() == GameMode.SPECTATOR;
    }

    /**
     * Removes the Clash item from the player’s inventory.
     */
    public void clearItem() {
        p.getInventory().clear(4);
    }

    /**
     * @return the ItemStack corresponding to the Clash item the player is
     *         currently holding, or null if it doesn’t exist
     */
    public ItemStack getItem() {
        return p.getInventory().getItem(4);
    }

    /**
     * @param item the ItemStack to set as the player’s current Clash item
     */
    public void setItem(ItemStack item) {
        p.getInventory().setItem(4, item);
    }

    /**
     * @return the player’s current Clash damage (‘% value’)
     */
    public int getDamage() {
        return damage;
    }

    /**
     * @param damage the player’s new Clash damage (‘% value’)
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * @return Given DoubleJump instance
     */
    public DoubleJump getDoubleJump() {
        return doubleJump;
    }

    /**
     * @return Given Energy instance
     */
    public Energy getEnergy() {
        return energy;
    }

    /**
     * Please use this method instead of Bukkit's, as Bukkit's method will
     * <em>always</em> return false.
     * @return whether the player is trying to sneak
     */
    public boolean isSneaking() {
        return isSneaking;
    }

    /**
     * Sets the player’s sneaking status as managed by Clash.
     * @param value whether the player should be sneaking
     */
    public void setSneaking(boolean value) {
        isSneaking = value;
    }

    /**
     * Sets the CLASH player's max. live, mainly used by matches
     * @param lives Max. live to set
     */
    public void setMaxLives(int lives) {
        maxLives = lives;
        p.setHealthScale(lives * 2);
    }

    /**
     * Gets the CLASH player's max. live, mainly used by matches
     */
    public int getMaxLives() {
        return maxLives;
    }

    /**
     * Sets the CLASH player's lives, mainly used by matches
     * @param lives Max. live to set
     */
    public void setLives(int lives) {
        this.lives = lives;
        if(lives > 0) {
            p.setHealth(20 / p.getHealthScale() * (lives * 2));
            return;
        }
        p.setHealth(1);
    }

    /**
     * Gets the CLASH player's lives, mainly used by matches
     */
    public int getLives() {
        return lives;
    }

}
