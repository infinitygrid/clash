package xyz.infinitygrid.clash;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.infinitygrid.clash.command.*;
import xyz.infinitygrid.clash.configuration.CfgLocations;
import xyz.infinitygrid.clash.configuration.CfgSigns;
import xyz.infinitygrid.clash.configuration.Configurations;
import xyz.infinitygrid.clash.effects.PlayerMovementListener;
import xyz.infinitygrid.clash.inventory.InteractiveInventoryManager;
import xyz.infinitygrid.clash.inventory.InvMatchSignCreator;
import xyz.infinitygrid.clash.inventory.ListenerInteractiveInventory;
import xyz.infinitygrid.clash.player.ListenerConnection;
import xyz.infinitygrid.clash.player.PlayerRegistry;
import xyz.infinitygrid.clash.player.behaviour.*;

public class CLASH extends JavaPlugin {

    public static CLASH instance;

    /**
     * Prints a message inside console
     *
     * @param msg Message to print
     */
    public static void sendConsoleMessage(String msg) {
        Bukkit.getConsoleSender().sendMessage("[CLASH] " + msg);
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        InteractiveInventoryManager.register(new InvMatchSignCreator());
        Bukkit.getOnlinePlayers().forEach(PlayerRegistry::registerCLPlayer);
        Configurations.register(new CfgLocations(), new CfgSigns());
        regListeners(new ListenerConnection(), new ListenerDoubleJump(), new ListenerGamemode(),
                new PlayerMovementListener(), new ListenerFoodLevel(), new ListenerToggleSneak(),
                new ListenerDamageEntity(), new ListenerDamageCause(), new ListenerDeath(),
                new ListenerExperience(), new ListenerSwapHandItems(), new ListenerRegainHealth(),
                new ListenerInteractiveInventory());
        regCommands(new CmdOperatorQuit(), new CmdOperatorSetspawn(), new CmdOperatorSpawnItem());
        sendConsoleMessage("Plugin has been enabled.");
    }

    @Override
    public void onDisable() {
        Bukkit.getOnlinePlayers().forEach(PlayerRegistry::unregisterCLPlayer);
        sendConsoleMessage("Plugin has been disabled.");
    }

    /**
     * Registers passed Listener classes
     */
    private void regListeners(Listener... listeners) {
        PluginManager pm = Bukkit.getPluginManager();
        for (Listener l : listeners) {
            pm.registerEvents(l, this);
        }
    }

    /**
     * Registers commands with CommandHandler.class as executor
     *
     * @param commandOperators Classes extending CommandOperator
     */
    private void regCommands(CommandOperator... commandOperators) {
        for (CommandOperator cmdOp : commandOperators) {
            String name = cmdOp.getCommandOperatorData().getName();
            try {
                this.getCommand(name).setExecutor(new CommandHandler(cmdOp));
            } catch (NullPointerException ex) {
                sendConsoleMessage(ChatColor.RED + "An exception has occurred whilst trying to register commands.\n" +
                        "Have you written it in the plugin.yml?");
                ex.printStackTrace();
            }
        }
    }




}
