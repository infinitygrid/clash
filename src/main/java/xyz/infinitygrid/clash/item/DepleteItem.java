package xyz.infinitygrid.clash.item;

import org.bukkit.inventory.ItemStack;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * After-use action that depletes the item’s durability by a set amount.
 * (Currently a constant 25%.)
 */
public class DepleteItem implements AfterUse {

    @Override
    public void invoke(CLPlayer player) {
        assert player != null;

        final ItemStack item = player.getItem();

        final short newDurability = (short) (
                item.getDurability() +
                        (short) (0.25 * item.getType().getMaxDurability())
        );
        item.setDurability(newDurability);
    }
}
