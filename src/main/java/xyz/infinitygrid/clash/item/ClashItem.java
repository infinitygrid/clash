package xyz.infinitygrid.clash.item;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import xyz.infinitygrid.clash.misc.items.Items;
import xyz.infinitygrid.clash.player.CLPlayer;

import static org.bukkit.ChatColor.*;

/**
 * A type of Clash item a player can get in a match.
 */
public abstract class ClashItem {
    private String displayName;
    private Material material;
    private byte occurrence;

    /**
     * @param displayName the name an item of this type has in a player’s
     *                    inventory
     * @param material    the material an item of this type is displayed as
     * @param occurrence  the weight that the item type has in random generation
     */
    ClashItem(String displayName, Material material, byte occurrence) {
        assert displayName != null;
        assert material != null;

        this.displayName = displayName;
        this.material = material;
        this.occurrence = occurrence;
    }

    // getters and setters

    public Material getMaterial() {
        return material;
    }

    public byte getOccurrence() {
        return occurrence;
    }

    public String getDisplayName() {
        return displayName;
    }

    public abstract byte getAmount();

    /**
     * @return the {@link AfterUse} object associated with this item type
     */
    public abstract AfterUse getAfterUse();

    // proper methods

    /**
     * Called when a player right-clicks with an item of this type in hand.
     * Default implementation does nothing and returns {@code false}.
     * @param clPlayer the player holding this item
     * @param block    if the player right-clicked air or an entity, null;
     *                 else the block that was right-clicked
     * @return whether the item was used, i.e. whether the after-use action
     * should be run
     */
    public boolean onRightClick(CLPlayer clPlayer, Block block) {
        assert clPlayer != null;
        return false;
    }

    /**
     * Called when a player left-clicks with an item of this type in hand.
     * Default implementation does nothing and returns {@code false}.
     * @param clPlayer the player holding this item
     * @param block    if the player left-clicked air or an entity, null;
     *                 else the block that was left-clicked
     * @return whether the item was used, i.e. whether the after-use action
     * should be run
     */
    public boolean onLeftClick(CLPlayer clPlayer, Block block) {
        assert clPlayer != null;
        return false;
    }

    /**
     * Called when a player hits another player with an item of this type in
     * hand.
     * Default implementation does nothing and returns {@code false}.
     * @param clPlayer the player holding this item
     * @param other    the player who was hit
     * @return whether the item was used, i.e. whether the after-use action
     * should be run
     */
    public boolean onHitPlayer(CLPlayer clPlayer, CLPlayer other) {
        assert clPlayer != null;
        assert other != null;
        return false;
    }

    /**
     * @return CLASH item as {@link org.bukkit.inventory.ItemStack} of 1 item
     */
    public ItemStack toItemStack() {
        return Items.builder()
                .setMaterial(material)
                .setDisplayName(GOLD + BOLD.toString() + displayName)
                .addItemFlag(ItemFlag.HIDE_ATTRIBUTES)
                .addDescriptionLine(BLUE + "CLASH Item")
                .build();
    }

}