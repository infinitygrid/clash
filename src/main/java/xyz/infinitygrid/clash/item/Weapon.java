package xyz.infinitygrid.clash.item;

import org.bukkit.Material;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * An item that deals damage to players hit with it.
 */
public class Weapon extends ClashItem {

    private final AfterUse afterUse = new DepleteItem();
    private final short damage;

    /**
     * @param displayName the name an item of this type has in a player’s
     *                    inventory
     * @param material    the material an item of this type is displayed as
     * @param occurrence  the weight that the item type has in random generation
     * @param damage      the amount of damage this weapon type deals
     */
    public Weapon(
            String displayName,
            Material material,
            byte occurrence,
            short damage
    ) {
        super(displayName, material, occurrence);
        this.damage = damage;
    }

    @Override
    public byte getAmount() { return 1; }

    @Override
    public AfterUse getAfterUse() { return afterUse; }

    @Override
    public boolean onHitPlayer(CLPlayer clPlayer, CLPlayer other) {
        assert clPlayer != null;
        assert other != null;

        final int oldDamage = other.getDamage();
        other.setDamage(oldDamage + this.damage);
        return true;
    }
}
