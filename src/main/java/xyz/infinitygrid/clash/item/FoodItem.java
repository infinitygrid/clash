package xyz.infinitygrid.clash.item;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * An item that restores health on right-click.
 */
public class FoodItem extends ClashItem {
    private final short healing;

    private final AfterUse afterUse = new ConsumeItem();

    /**
     * @param displayName the name an item of this type has in a player’s
     *                    inventory
     * @param material    the material an item of this type is displayed as
     * @param occurrence  the weight that the item type has in random generation
     * @param healing     the amount of damage this type of food heals
     */
    public FoodItem(
            String displayName,
            Material material,
            byte occurrence,
            short healing
    ) {
        super(displayName, material, occurrence);
        this.healing = healing;
    }

    @Override
    public byte getAmount() { return 1; }

    @Override
    public AfterUse getAfterUse() { return afterUse; }

    @Override
    public boolean onRightClick(CLPlayer clPlayer, Block block) {
        assert clPlayer != null;

        final int oldDamage = clPlayer.getDamage();
        if (oldDamage == 0) // if there’s nothing to heal:
            return false; // don’t consume

        final Player player = clPlayer.getBukkitPlayer();
        clPlayer.setDamage(Math.max(oldDamage - healing, 0));
        player.getWorld().playSound(
                player.getLocation(),
                Sound.ENTITY_PLAYER_BURP,
                1f, 1f
        );
        final Location eyeLoc = player.getEyeLocation();
        final Vector direction = eyeLoc.getDirection();
        player.getWorld().spawnParticle(
                Particle.ITEM_CRACK,
                eyeLoc.add(direction.getX() * 0.5, 0, direction.getZ() * 0.5),
                10, 0, 0, 0, .05f,
                clPlayer.getItem()
        );
        return true;
    }
}
