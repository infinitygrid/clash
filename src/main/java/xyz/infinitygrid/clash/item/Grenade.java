package xyz.infinitygrid.clash.item;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;

/**
 * A throwable item that explodes after a set amount of time.
 */
public class Grenade extends ThrowableItem {

    private final int fuseTime;
    private final float explosionStrength;

    /**
     * @param displayName       the name an item of this type has in a player’s
     *                          inventory
     * @param material          the material an item of this type is displayed
     *                          as
     * @param occurrence        the weight that the item type has in random
     *                          generation
     * @param fuseTime          the number of ticks after which the grenade
     *                          explodes
     * @param explosionStrength the strength of the ensuing explosion as used by
     *                          {@link World#createExplosion(Location, float)}
     */
    public Grenade(
            String displayName,
            Material material,
            byte occurrence,
            int fuseTime,
            float explosionStrength
    ) {
        super(displayName, material, occurrence);
        this.fuseTime = fuseTime;
        this.explosionStrength = explosionStrength;
    }

    @Override
    public void onThrow(Item thrown) {

        // TODO: particles and sounds and explosion

    }
}
