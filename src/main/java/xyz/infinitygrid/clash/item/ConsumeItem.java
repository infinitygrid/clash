package xyz.infinitygrid.clash.item;

import org.bukkit.inventory.ItemStack;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * After-use action that consumes one item from the item stack.
 */
public class ConsumeItem implements AfterUse {

    @Override
    public void invoke(CLPlayer player) {
        assert player != null;

        final ItemStack item = player.getItem();

        final int amount = item.getAmount();
        if (amount > 1)
            item.setAmount(amount - 1);
        else
            player.clearItem();
    }
}
