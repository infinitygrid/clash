package xyz.infinitygrid.clash.item;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * An item that is thrown in the direction the player looks on right-click.
 */
public abstract class ThrowableItem extends ClashItem {

    private final AfterUse afterUse = new ConsumeItem();

    /**
     * @param displayName the name an item of this type has in a player’s
     *                    inventory
     * @param material    the material an item of this type is displayed as
     * @param occurrence  the weight that the item type has in random generation
     */
    ThrowableItem(
            String displayName,
            Material material,
            byte occurrence
    ) {
        super(displayName, material, occurrence);
    }

    @Override
    public byte getAmount() { return 1; }

    @Override
    public AfterUse getAfterUse() { return afterUse; }

    @Override
    public boolean onRightClick(CLPlayer clPlayer, Block block) {
        assert clPlayer != null;

        final Player player = clPlayer.getBukkitPlayer();
        final Location eyeLoc = player.getEyeLocation();
        final Item thrown = player.getWorld().dropItem(
                eyeLoc,
                clPlayer.getItem()
        );
        thrown.setVelocity(eyeLoc.getDirection().multiply(1.3));
        thrown.setPickupDelay(Integer.MAX_VALUE);
        onThrow(thrown);
        return true;
    }


    /**
     * Called when the item has been thrown. Implementations should add sounds,
     * particles, and behaviour.
     * @param thrown    the item entity of the thrown item
     */
    protected abstract void onThrow(Item thrown);
}
