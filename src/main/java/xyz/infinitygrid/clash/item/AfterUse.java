package xyz.infinitygrid.clash.item;

import xyz.infinitygrid.clash.player.CLPlayer;

/**
 * An action to perform when an item has been used once – e.g. consume it or
 * decrease durability.
 */
public interface AfterUse {
    void invoke(CLPlayer player);
}