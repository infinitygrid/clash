package xyz.infinitygrid.clash.misc.items;

import org.bukkit.entity.EntityType;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SpawnEggMeta;

public class SpawnEggMetaBuilder extends ItemMetaBuilder {

    private SpawnEggMeta spawnEggMeta = (SpawnEggMeta) itemBuilder.buildCurrentItemMeta();
    private EntityType spawnEggType = null;

    SpawnEggMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Sets the entity type of a 'Spawn Egg'
     */
    public SpawnEggMetaBuilder setEntityEggType(EntityType entityEggType) {
        spawnEggType = entityEggType;
        return this;
    }

    @Override
    ItemMeta build() {
        if(spawnEggType != null) spawnEggMeta.setSpawnedType(spawnEggType);
        return spawnEggMeta;
    }
}
