package xyz.infinitygrid.clash.misc.items;

import org.bukkit.block.BlockState;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class BlockStateMetaBuilder extends ItemMetaBuilder {

    private BlockStateMeta blockStateMeta = (BlockStateMeta) itemBuilder.buildCurrentItemMeta();
    private BlockState blockState = null;

    BlockStateMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * @param blockState lock state to attach to item
     */
    public BlockStateMetaBuilder setBlockState(BlockState blockState) {
        this.blockState = blockState;
        return this;
    }

    @Override
    ItemMeta build() {
        if(blockState != null) blockStateMeta.setBlockState(blockState);
        return blockStateMeta;
    }

}
