package xyz.infinitygrid.clash.misc.items;

import org.bukkit.FireworkEffect;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FireworkMetaBuilder extends ItemMetaBuilder {

    private FireworkMeta fireworkMeta = (FireworkMeta) itemBuilder.buildCurrentItemMeta();
    private List<FireworkEffect> fireworkEffectList = new ArrayList<>();
    private int fireworkPower = -1;

    FireworkMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Adds a firework effect to firework item
     */
    public FireworkMetaBuilder addFireworkEffect(FireworkEffect fireworkEffect) {
        this.fireworkEffectList.add(fireworkEffect);
        return this;
    }

    /**
     * Adds firework effects to item
     */
    public FireworkMetaBuilder addFireworkEffects(FireworkEffect... fireworkEffect) {
        this.fireworkEffectList.addAll(Arrays.asList(fireworkEffect));
        return this;
    }

    /**
     * Adds firework effects to item
     */
    public FireworkMetaBuilder addFireworkEffects(Iterable<FireworkEffect> fireworkEffect) {
        fireworkEffect.forEach(fe -> this.fireworkEffectList.add(fe));
        return this;
    }

    /**
     * Sets the firework power of firework item
     */
    public FireworkMetaBuilder setFireworkPower(int power) {
        fireworkPower = power;
        return this;
    }

    @Override
    ItemMeta build() {
        if(fireworkEffectList.size() > 0) fireworkMeta.addEffects(fireworkEffectList);
        if(fireworkPower > -1) fireworkMeta.setPower(fireworkPower);
        return fireworkMeta;
    }

}
