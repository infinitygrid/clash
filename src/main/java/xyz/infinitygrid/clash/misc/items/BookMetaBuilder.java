package xyz.infinitygrid.clash.misc.items;

import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookMetaBuilder extends ItemMetaBuilder {

    private BookMeta bookMeta = (BookMeta) itemBuilder.buildCurrentItemMeta();
    private List<String> pages = new ArrayList<>();
    private String author = null;
    private BookMeta.Generation generation = null;
    private String title = null;

    BookMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Adds one or more book pages to items. Each {@code String} equals a book page.
     */
    public BookMetaBuilder addBookPage(String... pages) {
        this.pages.addAll(Arrays.asList(pages));
        return this;
    }

    /**
     * Set/Replace a specific book page.
     * @param page Page to replace/set
     * @param data Page text
     */
    public BookMetaBuilder setBookPage(int page, String data) {
        this.pages.set(page, data);
        return this;
    }

    /**
     * Sets book pages to item
     */
    public BookMetaBuilder setBookPages(String... pages) {
        this.pages = Arrays.asList(pages);
        return this;
    }

    /**
     * Sets book pages to item
     */
    public BookMetaBuilder setBookPages(List<String> pages) {
        this.pages = pages;
        return this;
    }

    /**
     * Sets the author of book item
     */
    public BookMetaBuilder setBookAuthor(String author) {
        this.author = author;
        return this;
    }

    /**
     * Sets the book generation to item
     */
    public BookMetaBuilder setBookGeneration(BookMeta.Generation generation) {
        this.generation = generation;
        return this;
    }

    /**
     * Sets the book title of book item
     */
    public BookMetaBuilder setBookTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    ItemMeta build() {
        if(title != null) bookMeta.setTitle(title);
        if(generation != null) bookMeta.setGeneration(generation);
        if(author != null) bookMeta.setAuthor(author);
        if(pages.size() > 0) bookMeta.setPages(pages);
        return bookMeta;
    }

}
