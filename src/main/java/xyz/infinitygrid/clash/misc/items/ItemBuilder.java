package xyz.infinitygrid.clash.misc.items;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Create and modify {@code ItemStack}'s
 */
public class ItemBuilder {

    private Material mat;
    private String name;
    private int amount;
    private List<String> description;
    private short data;
    private short durability;
    private HashMap<Enchantment, Integer> enchantments;
    private HashMap<Enchantment, Integer> unsafeEnchantments;
    private List<ItemFlag> itemFlags;
    private boolean unbreakable;

    private ItemMetaBuilder itemMetaBuilder = null;

    private List<NamespacedKey> recipes = new ArrayList<>();

    private Color leatherArmorColor = null;

    private boolean useMapMeta = false;
    private Color mapColor = null;
    private String locationName = null;
    private boolean scaling = false;

    private OfflinePlayer skullOwner = null;

    private EntityType spawnEggType = null;

    /**
     * Creates a new {@code ItemStack} from scratch
     */
    ItemBuilder() {
        mat = null;
        name = null;
        amount = -1;
        description = new ArrayList<>();
        data = (short) -1;
        durability = (short) -1;
        enchantments = new HashMap<>();
        unsafeEnchantments = new HashMap<>();
        itemFlags = new ArrayList<>();
        unbreakable = false;
    }

    /**
     * Modifies given {@code ItemStack}
     */
    ItemBuilder(ItemStack itemStack) {
        mat = itemStack.getType();
        amount = itemStack.getAmount();
        data = itemStack.getData().getData();
        durability = itemStack.getDurability();
        enchantments = (HashMap<Enchantment, Integer>) itemStack.getEnchantments();
        unbreakable = false;
        if(itemStack.hasItemMeta()) {
            ItemMeta itemMeta = itemStack.getItemMeta();
            name = itemMeta.getDisplayName();
            description = itemMeta.getLore();
            itemFlags = Arrays.asList(itemMeta.getItemFlags().toArray(new ItemFlag[0]));
            unbreakable = itemMeta.isUnbreakable();
        }
    }

    /**
     * Sets the item's material
     */
    public ItemBuilder setMaterial(Material material) {
        mat = material;
        return this;
    }

    /**
     * Sets the item's display name
     */
    public ItemBuilder setDisplayName(String displayName) {
        name = displayName;
        return this;
    }

    /**
     * Sets the item's amount on one stack
     */
    public ItemBuilder setAmount(int amount) {
        if(amount > 0) {
            this.amount = amount;
        } else {
            throw new UnsupportedOperationException("Amount requires a value above 0");
        }
        return this;
    }

    /**
     * Adds a lore to the item
     */
    public ItemBuilder addDescriptionLine(String descriptionLine) {
        description.add(descriptionLine);
        return this;
    }

    /**
     * Sets the lore to {@code List<String>}; each string equals a line of description/lore
     */
    public ItemBuilder setDescription(List<String> description) {
        this.description = description;
        return this;
    }

    /**
     * Sets the item's data
     */
    public ItemBuilder setData(short data) {
        if(data > -1) {
            this.data = data;
        } else {
            throw new UnsupportedOperationException("Data requires a value above 0");
        }
        return this;
    }

    /**
     * Sets the item's durability
     */
    public ItemBuilder setDurability(short durability) {
        if(durability > -1) {
            this.durability = durability;
        } else {
            throw new UnsupportedOperationException("Durability requires a value above -1");
        }
        return this;
    }

    /**
     * Adds an enchantment to the item
     */
    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        enchantments.put(enchantment, level);
        return this;
    }

    /**
     * Adds an unsafe enchantment to the item.
     * Uses {@link ItemStack#addUnsafeEnchantment(Enchantment, int)} from Bukkit
     */
    public ItemBuilder addUnsafeEnchantment(Enchantment unsafeEnchantment, int level) {
        unsafeEnchantments.put(unsafeEnchantment, level);
        return this;
    }

    /**
     * Adds an {@code ItemFlag} to the items
     */
    public ItemBuilder addItemFlag(ItemFlag itemFlag) {
        itemFlags.add(itemFlag);
        return this;
    }

    /**
     * Makes the item unbreakable, or not
     */
    public ItemBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    ItemMeta buildCurrentItemMeta() {
        ItemStack itemStack = mat != null ? data > 0 ?
                new ItemStack(mat, 1, data) : new ItemStack(mat) : new ItemStack(Material.STONE, 1, (short) 0);
        ItemMeta itemMeta = itemStack.getItemMeta();
        if(name != null) itemMeta.setDisplayName(name);
        if(description != null) itemMeta.setLore(description);
        itemMeta.addItemFlags(itemFlags.toArray(new ItemFlag[0]));
        itemMeta.setUnbreakable(unbreakable);
        return itemMeta;
    }

    /**
     * Sets a custom {@code ItemMeta} to {@code ItemStack} using {@link ItemMetaBuilder}
     */
    void setCustomItemMeta(ItemMetaBuilder itemMetaBuilder) {
        this.itemMetaBuilder = itemMetaBuilder;
    }

    /**
     * @return Builder for potion items
     */
    public PotionMetaBuilder getPotionMetaBuilder() {
        return new PotionMetaBuilder(this);
    }

    /**
     * Builder for banner items
     */
    public BannerMetaBuilder getBannerMetaBuilder() {
        return new BannerMetaBuilder(this);
    }

    /**
     * Builder for items to hold block states
     */
    public BlockStateMetaBuilder getBlockStateMetaBuilder() {
        return new BlockStateMetaBuilder(this);
    }

    /**
     * Builder for book items
     */
    public BookMetaBuilder getBookMetaBuilder() {
        return new BookMetaBuilder(this);
    }

    /**
     * Builder for items that can store enchantments, not the ones that can be enchanted.
     * Example: Enchanted Book. They store enchantments.
     */
    public EnchantmentStorageMetaBuilder getEnchantmentStorageMetaBuilder() {
        return new EnchantmentStorageMetaBuilder(this);
    }

    /**
     * Builder for firework items
     */
    public FireworkMetaBuilder getFireworkMetaBuilder() {
        return new FireworkMetaBuilder(this);
    }

    /**
     * Builder for item {@link Material#KNOWLEDGE_BOOK}
     */
    public KnowledgeBookMetaBuilder getKnowledgeBookMetaBuilder() {
        return new KnowledgeBookMetaBuilder(this);
    }

    /**
     * Builder for colorizing leather armor
     */
    public LeatherArmorMetaBuilder getLeatherArmorMetaBuilder() {
        return new LeatherArmorMetaBuilder(this);
    }

    /**
     * Builder for map items
     */
    public MapMetaBuilder getMapMetaBuilder() {
        return new MapMetaBuilder(this);
    }

    /**
     * Builder to attach an OfflinePlayer to player skull item
     */
    public SkullMetaBuilder getSkullMetaBuilder() {
        return new SkullMetaBuilder(this);
    }

    /**
     * Builder for 'Spawn Egg' items
     */
    public SpawnEggMetaBuilder getSpawnEggMetaBuilder() {
        return new SpawnEggMetaBuilder(this);
    }

    /**
     * Builds the item and returns {@code ItemStack}
     */
    public ItemStack build() {
        ItemStack itemStack = mat != null ? data > 0 ?
                new ItemStack(mat, 1, data) : new ItemStack(mat) : new ItemStack(Material.STONE, 1, (short) 0);
        ItemMeta itemMeta = buildCurrentItemMeta();
        itemStack.setAmount(amount > 0 ? amount : 1);
        if(durability > -1) itemStack.setDurability(durability);
        itemStack.addEnchantments(enchantments);
        itemStack.addUnsafeEnchantments(unsafeEnchantments);
        itemStack.setItemMeta(itemMetaBuilder != null ? itemMetaBuilder.build() : itemMeta);
        return itemStack;
    }



}
