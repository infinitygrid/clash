package xyz.infinitygrid.clash.misc.items;

import org.bukkit.inventory.meta.ItemMeta;

/**
 * Builds item meta for classes in Bukkit extending {@link ItemMeta}
 */
abstract class ItemMetaBuilder {

    ItemBuilder itemBuilder;

    ItemMetaBuilder(ItemBuilder itembuilder) {
        this.itemBuilder = itembuilder;
    }

    /**
     * @return completed ItemMeta
     */
    abstract ItemMeta build();


    /**
     * Simply continue building the rest of the item
     * @return ItemBuilder you switched from whilst using it
     */
    public ItemBuilder parent() {
        itemBuilder.setCustomItemMeta(this);
        return itemBuilder;
    }

}
