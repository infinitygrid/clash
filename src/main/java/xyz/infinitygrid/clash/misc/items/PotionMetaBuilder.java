package xyz.infinitygrid.clash.misc.items;

import org.bukkit.Color;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;

import java.util.LinkedHashMap;

/**
 * Builder for {@link PotionMeta}
 */
public class PotionMetaBuilder extends ItemMetaBuilder {

    private PotionMeta potionMeta = (PotionMeta) itemBuilder.buildCurrentItemMeta();
    private PotionData basePotionData = null;
    private LinkedHashMap<PotionEffect, Boolean> customEffects = new LinkedHashMap<>();
    private Color potionColor = null;

    /**
     * Builder for {@link PotionMeta}
     */
    PotionMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Utilizes {@link PotionMeta#setBasePotionData(PotionData)}
     */
    public PotionMetaBuilder setBasePotionData(PotionData data) {
        basePotionData = data;
        return this;
    }

    /**
     * Utilizes {@link PotionMeta#addCustomEffect(PotionEffect, boolean)}}
     */
    public PotionMetaBuilder addCustomEffect(PotionEffect potionEffect, boolean overwrite) {
        customEffects.put(potionEffect, overwrite);
        return this;
    }

    /**
     * Utilizes {@link PotionMeta#setColor(Color)}
     */
    public PotionMetaBuilder setColor(Color color) {
        potionColor = color;
        return this;
    }

    public PotionMeta build() {
        if(basePotionData != null) potionMeta.setBasePotionData(basePotionData);
        if(customEffects.entrySet().size() > 0) customEffects.forEach(potionMeta::addCustomEffect);
        if(potionColor != null) potionMeta.setColor(potionColor);
        return potionMeta;
    }

}
