package xyz.infinitygrid.clash.misc.items;

import org.bukkit.block.banner.Pattern;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BannerMetaBuilder extends ItemMetaBuilder {

    private BannerMeta bannerMeta = (BannerMeta) itemBuilder.buildCurrentItemMeta();
    private List<Pattern> patterns = new ArrayList<>();

    BannerMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Adds one or more patterns to the banner using varargs
     * @param patterns Patterns to add
     */
    public BannerMetaBuilder addPattern(Pattern... patterns) {
        this.patterns.addAll(Arrays.asList(patterns));
        return this;
    }

    /**
     * Modify a specific pattern object by list index
     * @param pattern Pattern to modify
     */
    public BannerMetaBuilder setPattern(int index, Pattern pattern) {
        patterns.set(index, pattern);
        return this;
    }

    /**
     * Sets the banner patterns
     */
    public BannerMetaBuilder setPatterns(List<Pattern> patterns) {
        this.patterns = patterns;
        return this;
    }

    @Override
    ItemMeta build() {
        if(patterns.size() > 0) bannerMeta.setPatterns(patterns);
        return bannerMeta;
    }

}
