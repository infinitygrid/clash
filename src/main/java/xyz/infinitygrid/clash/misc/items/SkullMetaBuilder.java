package xyz.infinitygrid.clash.misc.items;

import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullMetaBuilder extends ItemMetaBuilder {

    private SkullMeta skullMeta = (SkullMeta) itemBuilder.buildCurrentItemMeta();
    private OfflinePlayer skullOwner = null;

    SkullMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Applies on OfflinePlayer to a skull block/item
     */
    public SkullMetaBuilder setSkullOwner(OfflinePlayer offlinePlayer) {
        skullOwner = offlinePlayer;
        return this;
    }

    @Override
    ItemMeta build() {
        if(skullOwner != null) skullMeta.setOwningPlayer(skullOwner);
        return skullMeta;
    }
}
