package xyz.infinitygrid.clash.misc.items;

import org.bukkit.Color;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.MapMeta;

public class MapMetaBuilder extends ItemMetaBuilder {

    private MapMeta mapMeta = (MapMeta) itemBuilder.buildCurrentItemMeta();
    private Color mapColor = null;
    private String locationName = null;
    private boolean scaling = mapMeta.isScaling();

    MapMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Sets the map color
     */
    public MapMetaBuilder setMapColor(Color color) {
        mapColor = color;
        return this;
    }

    /**
     * Sets the location name
     */
    public MapMetaBuilder setMapLocationName(String name) {
        locationName = name;
        return this;
    }

    /**
     * Sets if the map is scaling or not
     */
    public MapMetaBuilder setMapScaling(boolean scaling) {
        this.scaling = scaling;
        return this;
    }

    @Override
    ItemMeta build() {
        if(mapColor != null) mapMeta.setColor(mapColor);
        if(locationName != null) mapMeta.setLocationName(locationName);
        if(scaling != mapMeta.isScaling()) mapMeta.setScaling(scaling);
        return mapMeta;
    }

}
