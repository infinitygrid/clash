package xyz.infinitygrid.clash.misc.items;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.KnowledgeBookMeta;

import java.util.ArrayList;
import java.util.List;

public class KnowledgeBookMetaBuilder extends ItemMetaBuilder {

    private KnowledgeBookMeta knowledgeBookMeta = (KnowledgeBookMeta) itemBuilder.buildCurrentItemMeta();
    private List<NamespacedKey> recipes = new ArrayList<>();

    KnowledgeBookMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Adds recipe to knowledge book
     */
    public KnowledgeBookMetaBuilder addRecipe(NamespacedKey recipe) {
        recipes.add(recipe);
        return this;
    }

    /**
     * Set recipes of knowledge book
     */
    public KnowledgeBookMetaBuilder setRecipes(List<NamespacedKey> recipes) {
        this.recipes = recipes;
        return this;
    }

    @Override
    ItemMeta build() {
        if(recipes.size() > 0) knowledgeBookMeta.setRecipes(recipes);
        return knowledgeBookMeta;
    }

}
