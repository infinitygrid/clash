package xyz.infinitygrid.clash.misc.items;

import org.bukkit.Color;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class LeatherArmorMetaBuilder extends ItemMetaBuilder {

    private LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemBuilder.buildCurrentItemMeta();
    private Color color = null;

    LeatherArmorMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Change the color of leather armor
     */
    public LeatherArmorMetaBuilder setLeatherArmorColor(Color color) {
        this.color = color;
        return this;
    }

    @Override
    ItemMeta build() {
        if(color != null) leatherArmorMeta.setColor(color);
        return leatherArmorMeta;
    }

}
