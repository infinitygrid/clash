package xyz.infinitygrid.clash.misc.items;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class EnchantmentStorageMetaBuilder extends ItemMetaBuilder {

    private EnchantmentStorageMeta enchantmentStorageMeta = (EnchantmentStorageMeta) itemBuilder.buildCurrentItemMeta();
    private List<StoredEnchantData> storedEnchantDataList = new ArrayList<>();

    EnchantmentStorageMetaBuilder(ItemBuilder itembuilder) {
        super(itembuilder);
    }

    /**
     * Represents the data of stored enchant of {@link EnchantmentStorageMeta}
     */
    protected class StoredEnchantData {
        final Enchantment enchantment;
        final int level;
        final boolean ignoreLevelRestriction;
        StoredEnchantData(Enchantment enchantment, int level, boolean ignoreLevelRestriction) {
            this.enchantment = enchantment;
            this.level = level;
            this.ignoreLevelRestriction = ignoreLevelRestriction;
        }
    }

    /**
     * Adds a stored enchant to item
     * @param enchantment Enchantment to add
     * @param level Level of enchantment
     * @param ignoreLevelRestriction whether level should be accepted regardless of level restriction
     */
    public EnchantmentStorageMetaBuilder addStoredEnchant(Enchantment enchantment, int level,
                                                          boolean ignoreLevelRestriction) {
        storedEnchantDataList.add(new StoredEnchantData(enchantment, level, ignoreLevelRestriction));
        return this;
    }

    @Override
    ItemMeta build() {
        if(storedEnchantDataList.size() > 0) {
            storedEnchantDataList.forEach(sed ->
                    enchantmentStorageMeta.addStoredEnchant(sed.enchantment, sed.level, sed.ignoreLevelRestriction));
        }
        return enchantmentStorageMeta;
    }
}
