package xyz.infinitygrid.clash.misc;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.item.ClashItem;
import xyz.infinitygrid.clash.match.MatchContext;


public class StringConverter {

    private final String str;

    public StringConverter(String str) {
        this.str = str;
    }

    /**
     * @return if string is a player, using {@link org.bukkit.Bukkit#getPlayer(String)}
     */
    public boolean isPlayer() {
        return Bukkit.getPlayer(str) != null;
    }

    /**
     * @return a player, using {@link org.bukkit.Bukkit#getPlayer(String)}
     */
    public Player toPlayer() {
        return Bukkit.getPlayer(str);
    }

    /**
     * @return if string can be parsed to an integer, using {@link java.lang.Integer#parseInt(String)}. Method will
     * always return {@code false} if the length of the string == 0.
     */
    public boolean isInteger() {
        if(str.length() == 0) return false;
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    /**
     * @return returns the string, but parsed to an int, using {@link java.lang.Integer#parseInt(String)}. Method will
     * always return {@code null} if the length of the string == 0.
     */
    public Integer toInteger() {
        if(str.length() == 0) return null;
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * @return if the string can be parsed to a {@link java.lang.Double}.
     */
    public boolean isDouble() {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException | NullPointerException ex) {
            return false;
        }
    }

    /**
     * @return the string parsed to a double {@link java.lang.Double}. Will return {@code null} if the string cannot
     * be parsed to a double.
     */
    public Double toDouble() {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException | NullPointerException ex) {
            return null;
        }
    }

    /**
     * @return if the {@code String.equalsIgnoreCase} the name of a CLASH item, as defined in
     * {@link MatchContext#ITEMS}
     */
    public boolean isCLASHItem() {
        for(ClashItem clashItem : MatchContext.ITEMS) {
            if(str.replace("_", " ").equalsIgnoreCase(clashItem.getDisplayName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return a ClashItem defined from {@link MatchContext#ITEMS}, if the display name equals the string. Not
     * case-sensitive.
     */
    public ClashItem toCLASHItem() {
        for(ClashItem clashItem : MatchContext.ITEMS) {
            if(str.replace("_", " ").equalsIgnoreCase(clashItem.getDisplayName())) {
                return clashItem;
            }
        }
        return null;
    }

}
