package xyz.infinitygrid.clash.inventory;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Represents an interactive inventory, utilizing inventories as windows and slots as buttons
 */
public abstract class InteractiveInventory {

    private final String title;
    private final ItemStack[] contents;
    private final int maxSize;

    InteractiveInventory() {
        title = getInventory().getTitle();
        contents = getInventory().getContents();
        this.maxSize = getMaxSize();
    }

    abstract Inventory getInventory();
    abstract int getMaxSize();

    /**
     * @return title of inventory
     */
    String getTitle() {
        return title;
    }

    void setSlot(int rawSlot, ItemStack item) {
        getInventory().setItem(rawSlot, item);
    }

    ItemStack getSlot(int rawSlot) {
        return getInventory().getItem(rawSlot);
    }

    /**
     * Called when InteractiveInventory is being clicked on
     */
    public abstract void onClick(int rawSlot, ClickType clickType);

    public void open(Player p) {
        Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST, title);
        inv.setContents(contents);
        p.openInventory(inv);
    }

    /**
     * Compares passed parameters with each other
     * @param obj InteractiveInventory or Inventory
     * @return true, if both inventory titles == each other
     */
    public static boolean equals(InteractiveInventory ii, Object obj) {
        if(obj instanceof InteractiveInventory) {
            return ((InteractiveInventory) obj).title.equals(ii.title);
        } else if(obj instanceof Inventory) {
            return ((Inventory) obj).getTitle().equals(ii.title);
        }
        return false;
    }

    /**
     * Compares inventory titles
     * @param obj InteractiveInventory or Inventory
     * @return true, if both this & obj's inventory titles == each other
     */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof InteractiveInventory) {
            return ((InteractiveInventory) obj).title.equals(this.title);
        } else if(obj instanceof Inventory) {
            return ((Inventory) obj).getTitle().equals(title);
        }
        return false;
    }

}
