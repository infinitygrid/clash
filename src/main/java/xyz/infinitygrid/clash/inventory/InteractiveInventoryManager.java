package xyz.infinitygrid.clash.inventory;

import org.bukkit.inventory.Inventory;

import java.util.HashMap;

/**
 * Manages all interactive inventories
 */
public class InteractiveInventoryManager {

    private static HashMap<Class, InteractiveInventory> classInteractiveInventoryList = new HashMap<>();

    /**
     * Registers an interactive inventory
     * @param interactiveInventories Interactive inventories to register
     */
    public static void register(InteractiveInventory... interactiveInventories) {
        for(InteractiveInventory interactiveInventory : interactiveInventories) {
            classInteractiveInventoryList.put(interactiveInventory.getClass(), interactiveInventory);
        }
    }

    /**
     * @return true, if inventory is registered as interactive inventory; false otherwise
     */
    public static boolean isInteractiveInventory(Inventory inventory) {
        return classInteractiveInventoryList.entrySet().stream().anyMatch(es ->
                es.getValue().getTitle().equals(inventory.getTitle())
        );
    }

    public static boolean isInteractiveInventory(Class c) {
        return classInteractiveInventoryList.containsKey(c);
    }

    public static InteractiveInventory get(Inventory inventory) {
        return classInteractiveInventoryList.entrySet().stream()
                .filter(es ->
                es.getValue().getTitle().equals(inventory.getTitle()))
                .findFirst()
                .get()
                .getValue();
    }

    public static InteractiveInventory get(Class c) {
        return classInteractiveInventoryList.get(c);
    }

}
