package xyz.infinitygrid.clash.inventory;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;

public class ListenerInteractiveInventory implements Listener {

    @EventHandler private void onInvInteract(InventoryClickEvent e) {
        Inventory clicked = e.getClickedInventory();
        if(InteractiveInventoryManager.isInteractiveInventory(clicked)) {
            InteractiveInventoryManager.get(clicked).onClick(e.getRawSlot(), e.getClick());
        }
        e.setCancelled(true);
    }

    @EventHandler private void onInvOpen(InventoryOpenEvent e) {

    }

    @EventHandler private void onInvClose(InventoryCloseEvent e) {

    }

}
