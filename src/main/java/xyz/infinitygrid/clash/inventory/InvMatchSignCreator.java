package xyz.infinitygrid.clash.inventory;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;

import static org.bukkit.ChatColor.BOLD;

public class InvMatchSignCreator extends InteractiveInventory {

    @Override
    Inventory getInventory() {
        Inventory inv = Bukkit.createInventory(null, 9, BOLD + "Match Sign Creator");
        return inv;
    }

    @Override
    int getMaxSize() {
        return 9001;
    }

    @Override
    public void onClick(int rawSlot, ClickType clickType) {

    }


}
