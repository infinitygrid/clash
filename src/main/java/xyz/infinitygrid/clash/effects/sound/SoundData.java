package xyz.infinitygrid.clash.effects.sound;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * A class that includes Bukkit's 'Sound' enum, volume and pitch
 */
public class SoundData {

    private final Sound s;
    private float vol = 1F;
    private float pit = 1F;

    /**
     * @param s Sound
     * @param vol Volume
     * @param pit Pitch
     */
    public SoundData(Sound s, float vol, float pit) {
        this.s = s;
        this.vol = vol;
        this.pit = pit;
    }

    /**
     * @param s Sound
     * @param vol Volume
     */
    public SoundData(Sound s, float vol) {
        this.s = s;
        this.vol = vol;
    }

    /**
     * @param s Sound
     */
    public SoundData(Sound s) {
        this.s = s;
    }

    /**
     * @return Sound
     */
    public Sound getSound() {
        return s;
    }

    /**
     * @return Volume
     */
    public float getVolume() {
        return vol;
    }

    /**
     * @return Pitch
     */
    public float getPitch() {
        return pit;
    }

    /**
     * Plays the sound to a player
     * @param p Player
     */
    public void play(Player p) {
        p.playSound(p.getLocation(), s, vol, pit);
    }

}
