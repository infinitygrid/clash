package xyz.infinitygrid.clash.effects.sound;

import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import xyz.infinitygrid.clash.CLASH;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for playing sounds to a set of players at once.
 */
public class SoundFX {

    private List<Player> receiver;

    /**
     * @param receiver list of players who receive sounds played from this
     *                 object
     */
    public SoundFX(List<Player> receiver) {
        this.receiver = receiver;
    }

    /**
     * @param receiver a single player who receives sounds played from this
     *                 object
     */
    public SoundFX(Player receiver) {
        this.receiver = new ArrayList<>();
        this.receiver.add(receiver);
    }

    /**
     * @param receiver all players who are in this world at the time of the call
     *                 will receive sounds played from this object
     */
    public SoundFX(World receiver) {
        this.receiver = receiver.getPlayers();
    }

    public void soundDrop(Sound s, float vol, float fromPitch, float toPitch, int times) {
        new BukkitRunnable() {
            int runCount = 0;
            float pitch = fromPitch;
            @Override
            public void run() {
                if (runCount >= times) {
                    cancel();
                    return;
                }
                // proper lineär interpolation, none of this ‘counting with a
                // float’ nonsense
                final float t = (float) runCount / times;
                final float oneMinusT = (float) (times - runCount) / times;
                pitch = fromPitch * oneMinusT + toPitch * t;
                play(s, vol, pitch);

                runCount++;
            }
        }.runTaskTimer(CLASH.instance, 0, 1);
    }

    public void play(SoundData... soundDs) {
        for (SoundData soundData : soundDs) {
            play(
                    soundData.getSound(),
                    soundData.getVolume(),
                    soundData.getPitch()
            );
        }
    }

    private void play(Sound s, float vol, float pitch) {
        for (Player p : receiver) {
            p.playSound(p.getLocation(), s, vol, pitch);
        }
    }

}
