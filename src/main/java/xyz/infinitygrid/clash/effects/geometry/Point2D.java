package xyz.infinitygrid.clash.effects.geometry;

public class Point2D {

    public double x;
    public double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void add(double x, double y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }

}
