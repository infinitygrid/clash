package xyz.infinitygrid.clash.effects.geometry;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Used for generating shapes, mainly for coordinates to spawn particles on
 */
public abstract class Shape {

    protected double radius;
    protected float detail;
    private int coordCount = 0;

    private static final Cache<Integer, List<Point3D>> coordinateCache = CacheBuilder.newBuilder().maximumSize(10000).expireAfterWrite(5, TimeUnit.MINUTES).build();

    /**
     * Creates a specified shape with a specified radius and detail
     *
     * @param radius The radius of the shape
     * @param detail How much detail goes into generating the shape
     */
    Shape(double radius, float detail) {
        this.radius = radius;
        this.detail = detail;
    }

    /**
     *
     * @param r Sets the radius of the shape
     */
    public void setRadius(double r) {
        radius = r;
    }

    /**
     * @return Set radius of shape
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param d Sets the detail of the shape
     */
    public void setDetail(float d) {
        detail = d;
    }

    /**
     * @return Set detail of shape
     */
    public float getDetail() {
        return detail;
    }

    protected abstract List<Point3D> gen();

    /**
     * @return Generates and returns a list of coordinates forming the shape
     */
    public List<Point3D> get() {
        try {
            return coordinateCache.get(this.hashCode(), () -> {
                List<Point3D> coords = gen();
                coordCount = coords.size();
                coordinateCache.put(this.hashCode(), coords);
                return coords;
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape = (Shape) o;
        return Double.compare(shape.radius, radius) == 0 &&
                Float.compare(shape.detail, detail) == 0;
    }

    @Override
    public int hashCode() {
        int result = 43;
        long longRadius = Double.doubleToLongBits(radius);
        int c = (int) (longRadius ^ (longRadius >>> 32));
        c = c + Float.floatToIntBits(detail);
        return 31 * result + c;
    }

    public static class Sphere extends Shape {
        /**
         * Generates a sphere with specified radius and detail
         *
         * @param radius The radius of the sphere
         * @param detail How much detail goes into generating the shape
         */
        public Sphere(double radius, float detail) {
            super(radius, detail);
        }

        /**
         * @return List of coordinates for a sphere
         */
        // TODO: Inaccurate calculation here
        @Override
        @Deprecated
        public List<Point3D> gen() {
            List<Point3D> coords =
                    new ArrayList<>((int) Math.ceil(360 * detail));
            for (double x = -radius; x < +radius; x+=0.1) {
                for (double y = -radius; y < +radius; y+=0.1) {
                    for (double z = -radius; z < +radius; z+=0.1) {
                        double dist = Math.sqrt(Math.pow(0-x, 2) + Math.pow(0-y, 2) + Math.pow(0-z, 2));
                        if (dist >= radius-0.01 && dist <= radius+0.01) {
                            coords.add(new Point3D(x, y, z));
                        }
                    }
                }
            }
            return coords;
        }
    }

    public static class HorizontalCircle extends Shape {
        /**
         * Generates a horizontal circle with specified radius and detail
         *
         * @param radius The radius of the circle
         * @param detail How much detail goes into generating the shape
         */
        public HorizontalCircle(double radius, float detail) {
            super(radius, detail);
        }

        /**
         * @return Coordinates for a horizontal, hollow circle
         */
        @Override
        protected List<Point3D> gen() {
            List<Point3D> coordList =
                    new ArrayList<>((int) Math.ceil(360 * detail));
            for (int i = 0; i < 360 * detail; i++) {
                double x = radius*Math.cos(i/detail);
                double z = radius*Math.sin(i/detail);
                coordList.add(new Point3D(x, 0, z));
            }
            return coordList;
        }

    }
}
