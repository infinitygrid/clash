package xyz.infinitygrid.clash.effects.geometry;

public class Point3D {

    public double x;
    public double y;
    public double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void add(double x, double y, double z) {
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }

    @Override
    public int hashCode() {
        int result = 31;
        int c;
        long xL = Double.doubleToLongBits(x);
        long yL = Double.doubleToLongBits(y);
        long zL = Double.doubleToLongBits(z);
        c = (int) (xL ^ (xL >>> 32));
        c = c + (int) (yL ^ (yL >>> 32));
        c = c + (int) (zL ^ (zL >>> 32));
        return result = 31 * result + c;
    }
}
