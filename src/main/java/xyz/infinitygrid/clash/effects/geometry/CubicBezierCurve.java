package xyz.infinitygrid.clash.effects.geometry;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Outputs values for a Cubic-Bézier Curve
 */
public class CubicBezierCurve {

    private double t;

    private double ctrlP0x;
    private double ctrlP0y;
    private double ctrlP1x;
    private double ctrlP1y;

    private static final Point2D p0 = new Point2D(0, 0);
    private static final Point2D p1 = new Point2D(1, 1);

    /**
     * Creates a bezier curve with the first point at (0,0) and the last at (1,1)
     * @see <a href="http://cubic-bezier.com">http://cubic-bezier.com</a> to visualize a cubic-bézier curve.
     *
     * @param t Time/Progress, ranging from 0 to 1
     * @param ctrlP0x X of first control point
     * @param ctrlP0y Y of first control point
     * @param ctrlP1x X of second control point
     * @param ctrlP1y Y of second control point
     */
    public CubicBezierCurve(double t, double ctrlP0x, double ctrlP0y, double ctrlP1x, double ctrlP1y) {
        this.t = t;
        this.ctrlP0x = ctrlP0x;
        this.ctrlP0y = ctrlP0y;
        this.ctrlP1x = ctrlP1x;
        this.ctrlP1y = ctrlP1y;
    }

    /**
     * Sets the progress
     */
    public CubicBezierCurve setT(double t) {
        this.t = t;
        return this;
    }

    /**
     * Reverts the values from the cubic-bézier curve
     */
    public CubicBezierCurve revert() {
        ctrlP0x = 1 - ctrlP0x;
        ctrlP1x = 1 - ctrlP1x;
        ctrlP0y = 1 - ctrlP0y;
        ctrlP1y = 1 - ctrlP1y;
        return this;
    }

    private static final Cache<Integer, Point2D> curvePoint2DCache = CacheBuilder.newBuilder().maximumSize(10000).expireAfterWrite(5, TimeUnit.MINUTES).build();

    /**
     * @return Array of (first) X, progression and (secondly) Y, the time/progress
     */
    public Point2D get() {
        try {
            return curvePoint2DCache.get(this.hashCode(), () -> {
                Point2D point2D = new Point2D(
                        Math.pow(1 - t, 3) * p0.x + Math.pow(1 - t, 2) * 3 * t * ctrlP0x + (1 - t) * 3 * t * t * ctrlP1x + t * t * t * p1.x,
                        Math.pow(1 - t, 3) * p0.y + Math.pow(1 - t, 2) * 3 * t * ctrlP0y + (1 - t) * 3 * t * t * ctrlP1y + t * t * t * p1.y
                );
                curvePoint2DCache.put(this.hashCode(), point2D);
                return point2D;
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CubicBezierCurve) {
            CubicBezierCurve other = (CubicBezierCurve) obj;
            return this.t == other.t &&
                    this.ctrlP0x == other.ctrlP0x &&
                    this.ctrlP0y == other.ctrlP0y &&
                    this.ctrlP1x == other.ctrlP1x &&
                    this.ctrlP1y == other.ctrlP1y;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 97;
        long tLong = Double.doubleToLongBits(t);
        long ctrlP0xLong = Double.doubleToLongBits(ctrlP0x);
        long ctrlP0yLong = Double.doubleToLongBits(ctrlP0y);
        long ctrlP1xLong = Double.doubleToLongBits(ctrlP1x);
        long ctrlP1yLong = Double.doubleToLongBits(ctrlP1y);
        int c = (int) (tLong ^ (tLong >>> 32));
        c = c + ((int)(ctrlP0xLong ^ (ctrlP0xLong >>> 32)));
        c = c + ((int)(ctrlP0yLong ^ (ctrlP0yLong >>> 32)));
        c = c + ((int)(ctrlP1xLong ^ (ctrlP1xLong >>> 32)));
        c = c + ((int)(ctrlP1yLong ^ (ctrlP1yLong >>> 32)));
        result = 31 * result + c;
        return result;
    }

}
