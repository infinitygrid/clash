package xyz.infinitygrid.clash.effects;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.effects.geometry.CubicBezierCurve;

/**
 * Class used for 'effects' on the player GUI
 */
public class GUIFX {

    /**
     * For transitions on the experience bar
     */
    public static class ExperienceBarTransition {

        private final Player p;

        /**
         * @param p The player to do an experience bar transition on
         */
        public ExperienceBarTransition(Player p) {
            this.p = p;
        }

        BukkitTask transitionTask = null;

        /**
         * Does a transition from one value to the other on a players experience bar
         * @param from From value
         * @param to To Value
         * @param seconds Seconds it should take // TODO: Inaccurate
         * @param tickSpeed Tick speed
         */
        public void make(float from, float to, double seconds, double tickSpeed) {
            if(transitionTask == null) {
                p.setExp(from);
                boolean decrement = from > to;
                CubicBezierCurve cubicBezierCurve;
                cubicBezierCurve = new CubicBezierCurve(from,.09,.83,.15,.88);
                if (!decrement) cubicBezierCurve = cubicBezierCurve.revert();
                CubicBezierCurve finalCubicBezierCurve = cubicBezierCurve;
                transitionTask = Bukkit.getScheduler().runTaskTimerAsynchronously(CLASH.instance, new Runnable() {
                    final double i = seconds * (0.05*tickSpeed);
                    boolean done = false;
                    double t = from;
                    @Override
                    public void run() {
                        if(!done) {
                            if(decrement) {
                                if(t > to) {
                                    t-=i;
                                } else {
                                    done = true;
                                }
                            } else {
                                if(t < to) {
                                    t+=i;
                                } else {
                                    done = true;
                                }
                            }
                        } else {
                            t = to;
                            cancelTask();
                        }
                        float newExp = (float) finalCubicBezierCurve.setT(t).get().x;
                        if(newExp >= 0F && newExp <= 1F) p.setExp(newExp);
                    }
                }, 0, 1);
            } else {
                cancelTask();
                make(from, to, seconds, tickSpeed);
            }
        }

        public boolean inProgress() {
            return transitionTask != null;
        }

        /**
         * Cancels the animation task
         */
        private void cancelTask() {
            transitionTask.cancel();
            transitionTask = null;
        }

    }

}