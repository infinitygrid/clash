package xyz.infinitygrid.clash.effects;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import xyz.infinitygrid.clash.CLASH;
import xyz.infinitygrid.clash.effects.geometry.Point3D;
import xyz.infinitygrid.clash.effects.geometry.Shape;

import java.util.List;

/**
 * A class that is used for very cool effects on the world
 */
public class WorldFX {

    /**
     * The shochwave effect consists of multiple, hollow, horizontally placed circles that make it seem like
     * one circle that is expanding, creating a 'shockwave'
     */
    public static class Shockwave {

        private final Player p;
        private boolean running = false;

        /**
         * @param p The location of shockwave, specified from a player
         */
        public Shockwave(Player p) {
            this.p = p;
        }

        /**
         * Spawns the shockwave
         * @param loc Location the shockwave should be spawned to
         * @param particle The particle the shockwave should use
         * @param minRadius The radius the shockwave starts at
         * @param maxRadius The radius the shockwave expand to
         * @param detailRunCount The amount of each generation the shockwave goes through until it has reached the max.
         *                      radius. Each run equals 1 tick
         * @param tickSpeed The delay between runs
         * @param delay The delay before starting
         * @param particleSpeed Speed of the particles
         * @param visualDetail The detail of the shockwave
         * @param reverse Reverse-shockwave // TODO: Not done
         */
        public void make(Location loc, Particle particle, double minRadius,
                         double maxRadius, int detailRunCount, int tickSpeed, int delay,
                         float particleSpeed, float visualDetail, boolean reverse) {
            if (!running) {
                new BukkitRunnable() {
                    int runCount;
                    final double increment = (maxRadius - minRadius) / detailRunCount;
                    double shockwaveRadius = minRadius;
                    final Shape shape = new Shape.HorizontalCircle(shockwaveRadius, visualDetail);
                    final World world = p.getWorld();

                    @Override
                    public void run() {
                        if (runCount <= detailRunCount) {
                            if (!reverse) {
                                shape.setRadius(shockwaveRadius);
                                List<Point3D> cs = shape.get();
                                for (Point3D c : cs) {
                                    world.spawnParticle(particle, new Location(world, c.x + loc.getX(), c.y + loc.getY(), c.z + loc.getZ()), 1, 0, 0, 0, particleSpeed);
                                }
                                shockwaveRadius += increment;
                            } else {
                                // TODO: Do reverse here
                            }
                        } else {
                            cancel();
                            running = false;
                        }
                        runCount++;
                    }
                }.runTaskTimer(CLASH.instance, delay, tickSpeed);
                running = true;
            }
        }
    }

    /**
     * Particles that follow the player around until an event
     */
    public static class MovementTrail {

        private final Player p;
        private boolean increasingYRunning = false;
        private boolean decreasingYRunning = false;

        /**
         * @param p Player the trail should be following
         */
        public MovementTrail(Player p) {
            this.p = p;
        }

        /**
         * Spawns particles until a player falls downwards
         * @param pa Particle that should spawn
         * @param maxAmount The max. amount of particles to spawn
         * @param paSpeed The speed of the particles
         * @param tickSpeed The tick speed between each spawn
         */
        public void onIncreasingY(Particle pa, int maxAmount, double paSpeed, int tickSpeed) {
            if (!increasingYRunning) {
                increasingYRunning = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Location pastLoc = PlayerMovementListener.getPastLocation(p);
                        Location presentLoc = PlayerMovementListener.getPresentLocation(p);
                        if (pastLoc.getY() < presentLoc.getY()) {
                            p.spawnParticle(pa, presentLoc, maxAmount, .1, .1, .1, paSpeed);
                        } else {
                            cancel();
                            increasingYRunning = false;
                        }
                    }
                }.runTaskTimer(CLASH.instance, 2, tickSpeed);
            }
        }

        /**
         * Spawns particles until a players Y coordinate increases
         * @param pa Particle that should spawn
         * @param maxAmount The max. amount of particles to spawn
         * @param paSpeed The speed of the particles
         * @param tickSpeed The tick speed between each spawn
         */
        public void onDecreasingY(Particle pa, int maxAmount, double paSpeed, int tickSpeed) {
            if (!decreasingYRunning) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Location pastLoc = PlayerMovementListener.getPastLocation(p);
                        Location presentLoc = PlayerMovementListener.getPresentLocation(p);
                        if (pastLoc.getY() > presentLoc.getY() && !p.isOnGround()) {
                            p.spawnParticle(pa, presentLoc.add(0, -2, 0), maxAmount, .1, .1, .1, paSpeed);
                        } else {
                            cancel();
                            decreasingYRunning = false;
                        }
                    }
                }.runTaskTimer(CLASH.instance, 2, tickSpeed);
                decreasingYRunning = true;
            }
        }

    }

}
