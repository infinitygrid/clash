package xyz.infinitygrid.clash.effects;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Saves player location changes into HashMap's
 */
public class PlayerMovementListener implements Listener {

    private static final ConcurrentHashMap<Player, Location> pastLocation = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Player, Location> presentLocation = new ConcurrentHashMap<>();

    @EventHandler
    private void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        pastLocation.put(p, e.getFrom());
        presentLocation.put(p, e.getTo());
    }

    /**
     * @param p Player
     * @return Past location of player
     */
    public static Location getPastLocation(Player p) {
        return pastLocation.get(p);
    }

    /**
     * @param p Player
     * @return Current location of player
     */
    public static Location getPresentLocation(Player p) {
        return presentLocation.get(p);
    }

}
