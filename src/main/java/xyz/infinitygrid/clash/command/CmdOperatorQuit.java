package xyz.infinitygrid.clash.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.player.CLPlayer;
import xyz.infinitygrid.clash.player.PlayerRegistry;

import static org.bukkit.ChatColor.YELLOW;

/**
 * CommandOperator for /quit
 */
public class CmdOperatorQuit implements CommandOperator {

    @Override
    public CommandOperatorData getCommandOperatorData() {
        return new CommandOperatorData("quit", true);
    }

    @Override
    public boolean execute(CommandInfo cmd) {
        final CommandSender sender = cmd.getCommandSender();
        assert sender instanceof Player;
        CLPlayer clPlayer = PlayerRegistry.getCLPlayer((Player) sender);
        clPlayer.teleportToSpawn();
        clPlayer.getBukkitPlayer().sendMessage(YELLOW + "You've been teleported to the spawn!");
        return true;
    }

}
