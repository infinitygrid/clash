package xyz.infinitygrid.clash.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.item.ClashItem;
import xyz.infinitygrid.clash.misc.StringConverter;

import static org.bukkit.ChatColor.*;

/**
 * Uses CommandOperatorData in order to check if the command can be executed.
 */
public class CommandHandler implements CommandExecutor {

    private final CommandOperator operator;

    public CommandHandler(CommandOperator operator) {
        this.operator = operator;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String s, String[] args) {
        assert cs != null;
        CommandInfo commandInfo = new CommandInfo(cs, cmd, s, args);
        boolean execute = false;

        if(operator == null) return true;

        final CommandOperatorData commandOperatorData = operator.getCommandOperatorData();

        // Check if player passed the min. amount of required arguments
        final int minArgs = commandOperatorData.getMinArgs();
        if(minArgs > args.length) {
            cs.sendMessage(RED + "This command requires at least " + minArgs + " argument/s to be executed.");
            return true;
        }

        // Check if the player passed arguments that can be converted to specific types
        final Class[] reqArgs = commandOperatorData.getRequiredArguments();
        if(!argsFitRequirements(reqArgs, args)) {
            cs.sendMessage(RED + "Expected different arguments for command " + cmd.getName() + ":");
            cs.sendMessage("    " + RED + BOLD + UNDERLINE + "#" + " | " + RED + UNDERLINE + "Expected Type");
            for(int i = 0; i <= reqArgs.length-1; i++) {
                cs.sendMessage("    " + RED + BOLD + i + " | " + RED + reqArgs[i].getSimpleName());
            }
            return true;
        }

        // Check if player has permission to execute this command
        boolean hasPermission = true;
        if (commandOperatorData.requiresPermission()) {
            hasPermission = cs.hasPermission(commandOperatorData.getPermissionNode());
        }
        if (hasPermission) {
            if (cs instanceof Player) {
                execute = true;
            } else {
                if (!commandOperatorData.isPlayerOnly()) {
                    execute = true;
                } else {
                    cs.sendMessage(RED + "This command can only be executed by players.");
                }
            }
        } else {
            cs.sendMessage(RED + "You do not have the permission to execute this command.");
        }

        if (execute && !operator.execute(commandInfo)) commandInfo.showUsage();

        return true;
    }

    /**
     * Checks if the passed strings can be converted to any of the types in order, relative to given classes.
     * Currently supported are {@link org.bukkit.entity.Player}, {@link java.lang.Integer} and {@link java.lang.Double}.
     * @param reqArgs to which types the string should be converted to
     * @param args Strings
     * @return if arguments fit the requirements
     * @throws UnsupportedOperationException if any unsupported class is passed
     */
    private boolean argsFitRequirements(Class[] reqArgs, String[] args) {
        if(reqArgs != null)
        for(int i = 0; i <= reqArgs.length-1; i++) {
            final Class reqArg = reqArgs[i];
            final StringConverter strConv = new StringConverter(args[i]);

            if(reqArg == Integer.class) {
                if(!strConv.isInteger()) {
                    return false;
                }
            } else if(reqArg == Player.class) {
                if(!strConv.isPlayer()) {
                    return false;
                }
            } else if(reqArg == Double.class) {
                if(!strConv.isDouble()) {
                    return false;
                }
            } else if(reqArg == ClashItem.class) {
                if(!strConv.isCLASHItem()) {
                    return false;
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }
        return true;
    }

}
