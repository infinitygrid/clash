package xyz.infinitygrid.clash.command;

public interface CommandOperator {

    CommandOperatorData getCommandOperatorData();

    /**
     * Is run, when required checks by the given data of 'CommandOperatorData' pass 'CommandHandler'
     *
     * @return Return 'false', when command usage should be shown, and 'true', when not.
     */
    boolean execute(CommandInfo commandInfo);

}
