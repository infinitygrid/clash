package xyz.infinitygrid.clash.command;

import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import xyz.infinitygrid.clash.item.ClashItem;
import xyz.infinitygrid.clash.misc.StringConverter;

import static org.bukkit.ChatColor.GOLD;
import static org.bukkit.ChatColor.ITALIC;

public class CmdOperatorSpawnItem implements CommandOperator {

    @Override
    public CommandOperatorData getCommandOperatorData() {
        return new CommandOperatorData("spawnitem", true, "clash.spawnitem", ClashItem.class);
    }

    @Override
    public boolean execute(CommandInfo commandInfo) {
        final Player p = (Player) commandInfo.getCommandSender();
        StringConverter stringConverter = new StringConverter(commandInfo.getArguments()[0]);
        ClashItem clashItem = stringConverter.toCLASHItem();
        PlayerInventory pInv = p.getInventory();
        pInv.setItem(pInv.getHeldItemSlot(), clashItem.toItemStack());
        p.sendMessage(GOLD + "Here's your " + ITALIC + clashItem.getDisplayName());
        return true;
    }

}
