package xyz.infinitygrid.clash.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import static org.bukkit.ChatColor.RED;


/**
 * A 'summary object' of the parameters passed by Bukkit's onCommand() method of CommandExecutor interface.
 */
class CommandInfo {

    private final CommandSender commandSender;
    private final Command command;
    private final String label;
    private final String[] arguments;

    CommandInfo(CommandSender commandSender, Command command, String label, String[] arguments) {
        assert commandSender != null;
        this.commandSender = commandSender;
        this.command = command;
        this.label = label;
        this.arguments = arguments;
    }

    CommandSender getCommandSender() {
        return commandSender;
    }

    Command getCommand() {
        return command;
    }

    String getLabel() {
        return label;
    }

    String[] getArguments() {
        return arguments;
    }

    void showUsage() {
        commandSender.sendMessage("Usage: " + RED + "/" + command.getName());
    }

}
