package xyz.infinitygrid.clash.command;

/**
 * Data class used by CommandHandler to decide when the command should be executed
 */
public class CommandOperatorData {

    private final String name;
    private final boolean playerOnly;
    private final String permissionNode;
    private int minArgs;
    private Class[] arguments;

    /**
     * Data class used by CommandHandler to decide when the command should be executed
     *
     * @param name           Command name e.g. 'quit', 'flush', 'gamemode', etc.
     * @param playerOnly     'true' if the command should only be run by players
     */
    CommandOperatorData(String name, boolean playerOnly) {
        this.name = name;
        this.playerOnly = playerOnly;
        this.permissionNode = null;
    }

    /**
     * Data class used by CommandHandler to decide when the command should be executed
     *
     * @param name           Command name e.g. 'quit', 'flush', 'gamemode', etc.
     * @param playerOnly     'true' if the command should only be run by players
     * @param minArgs The required amount of arguments for the command to execute
     */
    CommandOperatorData(String name, boolean playerOnly, int minArgs) {
        this.name = name;
        this.playerOnly = playerOnly;
        this.minArgs = minArgs;
        this.permissionNode = null;
    }

    /**
     * Data class used by CommandHandler to decide when the command should be executed
     *
     * @param name           Command name e.g. 'quit', 'flush', 'gamemode', etc.
     * @param playerOnly     'true' if the command should only be run by players
     * @param permissionNode Sets required permission node for the command
     */
    CommandOperatorData(String name, boolean playerOnly, String permissionNode) {
        this.name = name;
        this.playerOnly = playerOnly;
        this.permissionNode = permissionNode;
    }

    /**
     * Data class used by CommandHandler to decide when the command should be executed
     *
     * @param name           Command name e.g. 'quit', 'flush', 'gamemode', etc.
     * @param playerOnly     'true' if the command should only be run by players
     * @param permissionNode Sets required permission node for the command
     * @param minArgs The required amount of arguments for the command to execute
     */
    CommandOperatorData(String name, boolean playerOnly, String permissionNode, int minArgs) {
        this.name = name;
        this.playerOnly = playerOnly;
        this.permissionNode = permissionNode;
        this.minArgs = minArgs;
    }

    /**
     * Data class used by CommandHandler to decide when the command should be executed
     *
     * @param name           Command name e.g. 'quit', 'flush', 'gamemode', etc.
     * @param playerOnly     'true' if the command should only be run by players
     * @param permissionNode Sets required permission node for the command
     * @param args Required arguments in order that can be cast to given arguments
     *             Supported classes: {@link org.bukkit.entity.Player},
     *             {@link java.lang.Integer}, {@link java.lang.Double}
     */
    CommandOperatorData(String name, boolean playerOnly, String permissionNode, Class... args) {
        this.name = name;
        this.playerOnly = playerOnly;
        this.permissionNode = permissionNode;
        this.minArgs = args.length;
        this.arguments = args;
    }

    /**
     * @return Command name e.g. 'quit', 'flush', 'gamemode', etc.
     */
    public String getName() {
        return name;
    }

    /**
     * @return 'true' when the command can only be run by players
     */
    boolean isPlayerOnly() {
        return playerOnly;
    }

    /**
     * @return Required permission node, if CommandOperatorData.requiresPermission(). Return 'null' instead if not required.
     */
    String getPermissionNode() {
        return permissionNode;
    }

    /**
     * @return Returns if the command requires permission or not.
     */
    boolean requiresPermission() {
        return permissionNode != null;
    }

    /**
     * @return the required amount of arguments for the command to execute
     */
    int getMinArgs() {
        return minArgs;
    }

    /**
     * @return the required arguments in given order that can be cast to returned objects
     */
    Class[] getRequiredArguments() {
        return arguments;
    }

}