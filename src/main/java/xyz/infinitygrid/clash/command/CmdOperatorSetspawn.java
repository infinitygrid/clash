package xyz.infinitygrid.clash.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.infinitygrid.clash.configuration.CfgLocations;
import xyz.infinitygrid.clash.configuration.Configurations;

import static org.bukkit.ChatColor.YELLOW;


/**
 * CommandOperator for /setspawn
 */
public class CmdOperatorSetspawn implements CommandOperator {

    @Override
    public CommandOperatorData getCommandOperatorData() {
        return new CommandOperatorData("setspawn", true, "clash.setspawn");
    }

    @Override
    public boolean execute(CommandInfo cmd) {
        final CommandSender sender = cmd.getCommandSender();
        assert sender instanceof Player;
        final Player p = (Player) sender;
        Configurations.getConfig(CfgLocations.class).writer().setLocation("Spawn", p.getLocation());
        p.sendMessage(YELLOW + "The spawn has been set relative to your location!");
        return true;
    }

}
